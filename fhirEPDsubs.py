#!/usr/bin/env python

#
# Generated Fri Aug 18 10:42:00 2023 by generateDS.py version 2.42.2.
# Python 3.10.12 (main, Jun 11 2023, 05:26:28) [GCC 11.4.0]
#
# Command line options:
#   ('-o', 'fhirEPD.py')
#   ('-s', 'fhirEPDsubs.py')
#
# Command line arguments:
#   EPDFHIR.xsd
#
# Command line:
#   generateDS.py -o "fhirEPD.py" -s "fhirEPDsubs.py" EPDFHIR.xsd
#
# Current working directory (os.getcwd()):
#   generateDS-2.42.2
#

import os
import sys
from lxml import etree as etree_

import fhirEPD as supermod
import prop_csv_and_masterEPD as pcam
import fhir4_cs_and_vs_EPD as f4csvs

def parsexml_(infile, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        parser = etree_.ETCompatXMLParser()
    try:
        if isinstance(infile, os.PathLike):
            infile = os.path.join(infile)
    except AttributeError:
        pass
    doc = etree_.parse(infile, parser=parser, **kwargs)
    return doc

def parsexmlstring_(instring, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        try:
            parser = etree_.ETCompatXMLParser()
        except AttributeError:
            # fallback to xml.etree
            parser = etree_.XMLParser()
    element = etree_.fromstring(instring, parser=parser, **kwargs)
    return element

#
# Globals
#

ExternalEncoding = ''
SaveElementTreeNode = True

#
# Data representation classes
#


class ValueSetSub(supermod.ValueSet, pcam.PropCsvAndMaster):
    def __init__(self, id=None, meta=None, extension=None, url=None, identifier=None, version=None, name=None, title=None, status=None, experimental=None, date=None, publisher=None, contact=None, description=None, jurisdiction=None, immutable=None, copyright=None, compose=None, **kwargs_):
        super(ValueSetSub, self).__init__(id, meta, extension, url, identifier, version, name, title, status, experimental, date, publisher, contact, description, jurisdiction, immutable, copyright, compose,  **kwargs_)



# ACHTUNG, AB HIER ELGA CODE (angepasst!)
    def get_resource(self):
        """Return resource type.

        In case of zuppl-creation self.resource will be set within fsh1.py#exportoSuppl()
        and that value should then be returned instead.
        """
        if hasattr(self,"resource") and (tmp := self.resource):
            return tmp
        else:
            return pcam.Resource.ValueSet.value

    def get_id(self):
        return f4csvs.get_id(super())

    #def get_implicitrules(self):
        #return f4csvs.get_implicitrules(super())

    # def get_language(self):
    #     return f4csvs.get_language(super())

    def get_text(self):
        return f4csvs.get_text(super())

    def get_contained(self):
        return f4csvs.get_contained(super())

    def get_extension(self):
        return f4csvs.get_extension(super())

    # def get_modifierextension(self):
    #     return f4csvs.get_modifierextension(super())

    def get_url(self):
        return f4csvs.get_url(super())

    def get_identifier(self):
        return f4csvs.get_identifier(super())

    def get_version(self):
        return f4csvs.get_version(super())

    def get_displayName(self):
        return f4csvs.get_displayName(super())

    def get_title(self):
        return f4csvs.get_title(super())

    def get_status(self):
        return f4csvs.get_Status(super())

    def get_experimental(self):
        return f4csvs.get_experimental(super())

    def get_date(self):
        return f4csvs.get_date(super())

    def get_publisher(self):
        return f4csvs.get_publisher(super())

    def get_contact(self):
        return f4csvs.get_contact(super())

    def get_description(self):
        return f4csvs.get_description(super())

    # def get_usecontext(self):
    #     return f4csvs.get_usecontext(super())

    def get_jurisdiction(self):
        return f4csvs.get_jurisdiction(super())

    def get_immutable(self):
        """Return ValueSet.immutable.
        """
        if tmp := super().get_immutable():
            return tmp.get_value()
        return None

    # def get_purpose(self):
    #     return f4csvs.get_purpose(super())

    def get_copyright(self):
        return f4csvs.get_copyright(super())

    def get_casesensitive(self):
        """Return CodeSystem.caseSensitive. Field from CodeSystem.

        Required because some exporto() (e.g. in prop_csv_and_master.py) do not check if resource is a CodeSystem or ValueSet.
        """
        return None

    def get_valueset(self):
        """Return CodeSystem.valueSet. Field from CodeSystem.

        Required because some exporto() (e.g. in prop_csv_and_master.py) do not check if resource is a CodeSystem or ValueSet.
        """
        return None

    def get_hierarchymeaning(self):
        """Return CodeSystem.hierarchyMeaning. Field from CodeSystem.

        Required because some exporto() (e.g. in prop_csv_and_master.py) do not check if resource is a CodeSystem or ValueSet.
        """
        return None

    def get_compositional(self):
        """Return CodeSystem.compositional. Field from CodeSystem.

        Required because some exporto() (e.g. in prop_csv_and_master.py) do not check if resource is a CodeSystem or ValueSet.
        """
        return None

    def get_versionneeded(self):
        """Return CodeSystem.versionNeeded. Field from CodeSystem.

        Required because some exporto() (e.g. in prop_csv_and_master.py) do not check if resource is a CodeSystem or ValueSet.
        """
        return None

    def get_content(self):
        """Return CodeSystem.content. Field from CodeSystem.
        """
        # In case of zuppl-creation self.content will be set within fsh1.py#exportoSuppl()
        if hasattr(self,"content") and (tmp := self.content):
            return tmp
        else:
            # Required because some exporto() (e.g. in prop_csv_and_master.py) do not check if resource is a CodeSystem or ValueSet.
            return None

    def get_supplements(self):
        """Return CodeSystem.supplements. Field from CodeSystem.
        """
        # In case of zuppl-creation self.supplements will be set within fsh1.py#exportoSuppl()
        if hasattr(self,"supplements") and (tmp := self.supplements):
            return tmp
        else:
            # Required because some exporto() (e.g. in prop_csv_and_master.py) do not check if resource is a CodeSystem or ValueSet.
            return None

    def get_count(self):
        """Return CodeSystem.count. Field from CodeSystem.

        Required because some exporto() (e.g. in prop_csv_and_master.py) do not check if resource is a CodeSystem or ValueSet.
        """
        return None

    def get_filter(self):
        """Return CodeSystem.filter. Field from CodeSystem.

        Required because some exporto() (e.g. in prop_csv_and_master.py) do not check if resource is a CodeSystem or ValueSet.
        """
        return []

    def get_property(self):
        """Return CodeSystem.property. Field from CodeSystem.

        Required because some exporto() (e.g. in prop_csv_and_master.py) do not check if resource is a CodeSystem or ValueSet.
        """
        return []

    def get_lockeddate(self):
        """Return ValueSet.compose.lockedDate.
        """
        if tmp := super().get_compose():
            if tmp := tmp.get_lockedDate():
                return tmp.get_value()
        return None

    def get_inactive(self):
        """Return ValueSet.compose.inactive.
        """
        if tmp := super().get_compose():
            if tmp := tmp.get_inactive():
                return tmp.get_value()
        return None

    def get_concept(self):
        """Processes ValueSet.compose.include, ValueSet.compose.exclude, and ValueSet.expansion.contains and
        returns a list of pcam.VSConcept.
        """

        # Once the concepts of the underlying ValueSet have been parsed, the result
        # will be accessible in self.concept (improve performance).
        if hasattr(self,"concept") and (tmp := self.concept):
            return tmp
        else:
            self.concept = []

            if tmp := super().get_compose():
                compose = tmp
                # parse ValueSet.compose.include
                if tmp := compose.get_include():
                    self.concept.extend(ValueSetSub.parse_include_information(tmp))

                # parse ValueSet.compose.exclude
                if tmp := compose.get_exclude():
                    self.concept.extend(ValueSetSub.parse_include_information(tmp, True))

            # based on the concepts processed so far, a dictionary with the
            # concept's code as key will be created in order to add information
            # which will be retrieved from ValueSet.expansion.contains
            concept_code_dict = self.buildConceptDictWithAttr('code', 'system')

            # parse ValueSet.expansion.contains for retrieving information about
            # possible hierarchies
            if tmp := super().get_expansion():
                if tmp := tmp.get_contains():
                    # create a new list that sorts the concepts exactly like the expand does
                    the_from_expand_sorted_list = []
                    # recrusivly parse the information in the contains
                    ValueSetSub.parse_expansion_contains_information(0, tmp, concept_code_dict, the_from_expand_sorted_list)
                    # subtract/delete all concepts from self.concept that are in the expand and add these concepts on the end of the list
                    # this is only done for the case, that an (invalid) fhir xml got more concepts in the include than in the expansion
                    self.concept = the_from_expand_sorted_list + list(set(self.concept) - set(the_from_expand_sorted_list))

            return self.concept

    def parse_include_information(include_list, exclude=False):
        """Parse the given include_list (of type ValueSet_Include, i.e. ValueSet.compose.include or ValueSet.compose.exclude)
        and transforms the information into pcam.VSConcepts.

        include_list -- List of ValueSet_Includes that should be transformed to pcam.VSConcepts().

        exclude -- If true all VSConcepts will represent exculde information.
        """
        the_concept_list = []

        for one_include in include_list:
            the_include_system = None
            the_include_version = None

            # retrieve ValueSet_Include.system
            if tmp := one_include.get_system():
                the_include_system = tmp.get_value()

            # retrieve ValueSet_Include.version
            if tmp := one_include.get_version():
                the_include_version = tmp.get_value()

            # check if either concepts or filters have been present for one_include
            include_all_valueset = True

            # retrieve ValueSet_Include.concept
            if tmp := one_include.get_concept():
                # at least one concept is present, hence, it is not a include all valueset
                include_all_valueset = False

                concept_list = tmp
                # for each concept within ValueSet_Include a pcam.VSConcept will be created
                for one_concept in concept_list:
                    the_concept = pcam.VSConcept()
                    the_concept.system = the_include_system
                    the_concept.version = the_include_version
                    the_concept.exclude = exclude

                    if tmp := one_concept.get_code():
                        the_concept.code = tmp.get_value()

                    if tmp := one_concept.get_display():
                        the_concept.display = tmp.get_value()

                    if tmp := one_concept.get_designation():
                        the_concept.designation = f4csvs.parse_concept_designation(tmp)

                    the_concept_list.append(the_concept)

            # retrieve ValueSet_Include.filter
            if tmp := one_include.get_filter():
                # at least one filter is present, hence, it is not a include all valueset
                include_all_valueset = False

                filter_list = tmp

                the_concept = pcam.VSConcept()
                the_concept.system = the_include_system
                the_concept.version = the_include_version
                the_concept.exclude = exclude

                # all filters that belong to this ValueSet_Include will be part
                # of one pcam.VSConcept
                for one_filter in filter_list:
                    the_filter = pcam.VSConcept.VSConceptFilter()

                    if tmp := one_filter.get_property():
                        the_filter.property = tmp.get_value()

                    if tmp := one_filter.get_op():
                        the_filter.op = tmp.get_value()

                    if tmp := one_filter.get_value():
                        the_filter.value = tmp.get_value()

                    the_concept.filter.append(the_filter)

                the_concept_list.append(the_concept)

            # NOT SUPPORTED ValueSet_Include.valueSet

            # if it is an "include all valueset" create a dummy VSConcept
            if include_all_valueset:
                the_concept = pcam.VSConcept()
                the_concept.system = the_include_system
                the_concept.version = the_include_version
                the_concept.exclude = exclude

                the_concept_list.append(the_concept)

        return the_concept_list

    def parse_expansion_contains_information(level, contains_list, concept_code_dict, the_from_expand_sorted_list, parent=None):
        """Parse the given contains_list (of type ValueSet_Contains, i.e. ValueSet.expansion.contains) and add
        information regarding possible hierarchies to the pcam.VSConcepts which are part of concept_code_dict.

        level -- the current level of hierarchy. Top level should have level=0.

        contains_list -- List of ValueSet_Contains of the current level.

        concept_code_dict -- Dictionary of all concepts (retrieved from ValueSet.compose.include and ValueSet.compose.exclude) with their code as key.

        the_from_expand_sorted_list -- the recrusivly build up list of concepts, occured in order of the expand

        parent -- the parent of the concepts within the contains_list.

        Currently, only information about a possible hierarchical ValueSet definition is being
        retrieved from the expansion. No other information - if available - will be processed.
        """
        for one_contains in contains_list:
            if (tmp_code := one_contains.get_code()) and (tmp_system := one_contains.get_system()):
                key = tmp_code.get_value() + tmp_system.get_value()
                # retrieve the concept from the dictionary
                if key in concept_code_dict.keys() and (the_concept := concept_code_dict[key]):
                    # set the concept's level
                    the_concept.level = level

                    # parse ValueSet.expansion.contains.abstract
                    if tmp := one_contains.get_abstract():
                        the_concept.abstract = tmp.get_value()

                    # parse ValueSet.expansion.contains.inactive
                    if tmp := one_contains.get_inactive():
                        the_concept.inactive = tmp.get_value()
                        if the_concept.inactive:
                            self.inactive = True

                    # if a parent has been specified add the_concept to the parent's member
                    if parent:
                        # setting theConcept as child of the parent
                        parent.member.append(the_concept)
                        # setting the parent of one_concept
                        the_concept.parent = parent

                    the_from_expand_sorted_list.append(the_concept)
                    # if one_contains contains a list of contains go into recursion
                    if tmp := one_contains.get_contains():
                        ValueSetSub.parse_expansion_contains_information(level+1, tmp, concept_code_dict, the_from_expand_sorted_list, the_concept)

    def exporto(self, outfile):
        """Write information to a FHIR ValueSet.

        self -- Object holding the information that should be written to the FHIR ValueSet.

        outfile -- File the information will be written to.
        """

        # Create new FHIR ValueSet
        valueset = ValueSetSub()

        # export general metadata
        f4csvs.exporto(self, globals(), valueset)

        if tmp := self.get_immutable():
            valueset.set_immutable(booleanSub(value = tmp))

        the_compose = None

        if tmp := self.get_lockeddate():
            the_compose = ValueSetSub.create_compose_if_not_existent(valueset)
            the_compose.set_lockedDate(dateSub(value=tmp))

        if tmp := self.get_inactive():
            the_compose = ValueSetSub.create_compose_if_not_existent(valueset)
            the_compose.set_inactive(booleanSub(value=tmp))

        if tmp := self.get_concept():
            the_compose = ValueSetSub.create_compose_if_not_existent(valueset)

            # create ValueSet.compose.include and/or ValueSet.compose.exclude
            do_expansion = ValueSetSub.exporto_include_exclude(self, the_compose, tmp)

            # if an expansion shall be appended to the ValueSet definition
            #if do_expansion:
                #the_expansion = ValueSet_ExpansionSub()

                # NOT SUPPORTED ValueSet_Expansion.identifier

                # set timestamp accordingly (required)
                #the_expansion.set_timestamp(dateTimeSub(value=datetime.now().strftime("%Y-%m-%dT%X.0000Z")))

                # NOT SUPPORTED ValueSet_Expansion.total

                # NOT SUPPORTED ValueSet_Expansion.offset

                # NOT SUPPORTED ValueSet_Expansion.parameter

                # the expansion will be created starting with those concepts which are at level=0 of a possible hierarchy
                #the_expansion.set_contains(ValueSetSub.exporto_expansion(self, list(filter(lambda concept: concept.level is not None and concept.level == 0, tmp))))

                #valueset.set_expansion(the_expansion)

        # export the FHIR ValueSet resource to the outfile
        valueset.export(outfile=outfile, level=0, namespacedef_='xmlns="http://hl7.org/fhir"')

    def create_compose_if_not_existent(valueset):
        """Create ValueSet.compose if it does not yet exist.

        valueset -- the ValueSet object
        """
        compose = None

        if not (compose := valueset.get_compose()):
            compose = composeTypeSub()
            valueset.set_compose(compose)

        return compose

    def exporto_include_exclude(self, the_compose, concept_list):
        """Export information from the given concept_list to the_compose and create
        ValueSet.compose.include and/or ValueSet.compose.exclude if necessary.

        the_compose -- Represents ValueSet.compose.
        concept_list -- List of pcam.VSConcepts which will be processed.
        """
        do_expansion = False

        # dictionaries containing the concepts for each system + version
        system_include_dict = {}
        system_exclude_dict = {}
        # dummy system + version if a VSConcept has got neither system nor version
        no_system_no_version = 'NO_SYSTEM_NO_VERSION'

        for one_concept in concept_list:
            exclude = one_concept.exclude

            # check if exclude is a valid value
            if exclude != True and exclude != False:
                sys.exit('Cannot determine if concept should be part of ValueSet.compose.include or ValueSet.compose.exclude')

            # based on exclude decide if the information within one_concept should be
            # added to ValueSet.compose.include ore ValueSet.compose.exclude
            the_system_dict = None
            if exclude:
                the_system_dict = system_exclude_dict
            else:
                the_system_dict = system_include_dict

            # retrieve system + version from one_concept
            the_system = one_concept.system
            # explicitly set the_version to None - otherwise it might occur that it has the value of a preceding loop-run
            the_version = None
            # write the system version if existent
            if tmp := one_concept.version:
                the_version = one_concept.version
            # else:
                # retrieve version from metadata

                # theoretically one_concept.system could be either canonical or OID in
                # most cases, however, it would be the canonical
                # if tmp := self.gainVersionFromCanonical(the_system):
                #     the_version = tmp
                # elif tmp := self.gainVersionFromOid(the_system):
                #     the_version = tmp
            # create key for the dictionary entry
            system_key = None
            if the_system or the_version:
                system_key = (the_system or 'None') + '#' + (the_version or 'None')
            else:
                system_key = no_system_no_version

            the_include = None
            # if the key does not yet exist in the dictionary a new ValueSet_Include
            # with the current system + version will be created
            if system_key not in the_system_dict:
                the_include = includeTypeSub()
                if the_system:
                    the_include.set_system(urlTypeSub(value=the_system))
                if the_version:
                    the_include.set_version(versionTypeSub(value=the_version))
                the_system_dict[system_key] = the_include
            else:
                the_include = the_system_dict[system_key]

            # if one_concept has got a code it will be transformed into a ValueSet_Concept (i.e. ValueSet.compose.include.concept or ValueSet.compose.exclude.concept)
            if tmp := one_concept.code:
                # if concepts are explicitly stated within ValueSet.compose.include a ValueSet.expansion will be
                # created with all those concepts from ValueSet.compose.include
                #
                # if exclude is false, do_expansion will become true
                # if exclude is true, do_expansion will retain its current value
                do_expansion |= not exclude

                the_concept = conceptTypeSub()

                the_concept.set_code(codeTypeSub(value=tmp))

                if tmp := one_concept.display:
                    the_concept.set_display(nameTypeSub(value=tmp))

                # if tmp := one_concept.designation:
                #     the_concept.set_designation(f4csvs.exporto_concept_designation(globals(), ValueSet_DesignationSub, tmp))

                the_include.add_concept(the_concept)
            # if one_concept has got filters they will be transformed into ValueSet_Filter (i.e. ValueSet.compose.include.filter or ValueSet.compose.exclude.filter)
            elif tmp := one_concept.filter:
                filter_list = tmp
                for one_filter in filter_list:
                    the_filter = ValueSet_FilterSub()

                    if tmp := one_filter.property:
                        the_filter.set_property(codeTypeSub(value=tmp))

                    if tmp := one_filter.op:
                        the_filter.set_op(FilterOperatorSub(value=tmp))

                    if tmp := one_filter.value:
                        the_filter.set_value(valueTypeSub(value=tmp))

                    the_include.add_filter(the_filter)

            # NOT SUPPORTED ValueSet_Include.valueSet

        # set ValueSet.compose.include
        the_compose.set_include(list(system_include_dict.values()))
        # set ValueSet.compose.exclude
        #the_compose.set_exclude(list(system_exclude_dict.values()))

        return do_expansion

    def exporto_expansion(self, concept_list):
        """Export information from the given concept_list to ValueSet.expansion.

        concept_list -- List of pcam.VSConcepts which will be processed.
        """
        the_concept_list = []

        for one_concept in concept_list:
            # only those concepts should be part of the expansion which have exclude=false
            if not one_concept.exclude:
                the_concept = ValueSet_ContainsSub()

                one_system = one_concept.system
                if one_system:
                    the_concept.set_system(urlTypeSub(value=one_system))

                if tmp := one_concept.abstract:
                    the_concept.set_abstract(booleanSub(value=tmp))

                if tmp := one_concept.inactive:
                    the_concept.set_inactive(booleanSub(value=tmp))

                # write the system version if existent
                if tmp := one_concept.version:
                    the_concept.set_version(versionTypeSub(value=tmp))
                else:
                    # retrieve version from metadata

                    # theoretically one_concept.system could be either canonical or OID in
                    # most cases, however, it would be the canonical
                    if tmp := self.gainVersionFromCanonical(one_system):
                        the_concept.set_version(versionTypeSub(value=tmp))
                    elif tmp := self.gainVersionFromOid(one_system):
                        the_concept.set_version(versionTypeSub(value=tmp))

                if tmp := one_concept.code:
                    the_concept.set_code(codeTypeSub(value=tmp))

                if tmp := one_concept.display:
                    the_concept.set_display(nameTypeSub(value=tmp))

                if tmp := one_concept.designation:
                    the_concept.set_designation(f4csvs.exporto_concept_designation(globals(), ValueSet_DesignationSub, tmp))

                # if one_concept contains any members a recursion will be started
                if tmp := one_concept.member:
                    the_concept.set_contains(ValueSetSub.exporto_expansion(self, tmp))

                the_concept_list.append(the_concept)

        return the_concept_list

supermod.ValueSet.subclass = ValueSetSub
# end class ValueSetSub

class idTypeSub(supermod.idType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(idTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.idType.subclass = idTypeSub
# end class idTypeSub


class metaTypeSub(supermod.metaType):
    def __init__(self, source=None, profile=None, **kwargs_):
        super(metaTypeSub, self).__init__(source, profile,  **kwargs_)
supermod.metaType.subclass = metaTypeSub
# end class metaTypeSub


class sourceTypeSub(supermod.sourceType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(sourceTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.sourceType.subclass = sourceTypeSub
# end class sourceTypeSub


class profileTypeSub(supermod.profileType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(profileTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.profileType.subclass = profileTypeSub
# end class profileTypeSub


class extensionTypeSub(supermod.extensionType):
    def __init__(self, url=None, valuePeriod=None, **kwargs_):
        super(extensionTypeSub, self).__init__(url, valuePeriod,  **kwargs_)
supermod.extensionType.subclass = extensionTypeSub
# end class extensionTypeSub


class valuePeriodTypeSub(supermod.valuePeriodType):
    def __init__(self, start=None, **kwargs_):
        super(valuePeriodTypeSub, self).__init__(start,  **kwargs_)
supermod.valuePeriodType.subclass = valuePeriodTypeSub
# end class valuePeriodTypeSub


class startTypeSub(supermod.startType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(startTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.startType.subclass = startTypeSub
# end class startTypeSub


class urlTypeSub(supermod.urlType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(urlTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.urlType.subclass = urlTypeSub
# end class urlTypeSub


class identifierTypeSub(supermod.identifierType):
    def __init__(self, use=None, system=None, value=None, **kwargs_):
        super(identifierTypeSub, self).__init__(use, system, value,  **kwargs_)
supermod.identifierType.subclass = identifierTypeSub
# end class identifierTypeSub


class useTypeSub(supermod.useType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(useTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.useType.subclass = useTypeSub
# end class useTypeSub


class systemTypeSub(supermod.systemType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(systemTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.systemType.subclass = systemTypeSub
# end class systemTypeSub


class valueTypeSub(supermod.valueType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(valueTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.valueType.subclass = valueTypeSub
# end class valueTypeSub


class versionTypeSub(supermod.versionType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(versionTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.versionType.subclass = versionTypeSub
# end class versionTypeSub


class nameTypeSub(supermod.nameType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(nameTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.nameType.subclass = nameTypeSub
# end class nameTypeSub


class titleTypeSub(supermod.titleType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(titleTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.titleType.subclass = titleTypeSub
# end class titleTypeSub


class statusTypeSub(supermod.statusType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(statusTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.statusType.subclass = statusTypeSub
# end class statusTypeSub


class experimentalTypeSub(supermod.experimentalType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(experimentalTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.experimentalType.subclass = experimentalTypeSub
# end class experimentalTypeSub


class dateTypeSub(supermod.dateType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(dateTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.dateType.subclass = dateTypeSub
# end class dateTypeSub


class publisherTypeSub(supermod.publisherType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(publisherTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.publisherType.subclass = publisherTypeSub
# end class publisherTypeSub


class contactTypeSub(supermod.contactType):
    def __init__(self, name=None, telecom=None, **kwargs_):
        super(contactTypeSub, self).__init__(name, telecom,  **kwargs_)
supermod.contactType.subclass = contactTypeSub
# end class contactTypeSub


class nameType1Sub(supermod.nameType1):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(nameType1Sub, self).__init__(value, valueOf_,  **kwargs_)
supermod.nameType1.subclass = nameType1Sub
# end class nameType1Sub


class telecomTypeSub(supermod.telecomType):
    def __init__(self, system=None, value=None, **kwargs_):
        super(telecomTypeSub, self).__init__(system, value,  **kwargs_)
supermod.telecomType.subclass = telecomTypeSub
# end class telecomTypeSub


class systemType2Sub(supermod.systemType2):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(systemType2Sub, self).__init__(value, valueOf_,  **kwargs_)
supermod.systemType2.subclass = systemType2Sub
# end class systemType2Sub


class valueType3Sub(supermod.valueType3):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(valueType3Sub, self).__init__(value, valueOf_,  **kwargs_)
supermod.valueType3.subclass = valueType3Sub
# end class valueType3Sub


class descriptionTypeSub(supermod.descriptionType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(descriptionTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.descriptionType.subclass = descriptionTypeSub
# end class descriptionTypeSub


class jurisdictionTypeSub(supermod.jurisdictionType):
    def __init__(self, coding=None, **kwargs_):
        super(jurisdictionTypeSub, self).__init__(coding,  **kwargs_)
supermod.jurisdictionType.subclass = jurisdictionTypeSub
# end class jurisdictionTypeSub


class codingTypeSub(supermod.codingType):
    def __init__(self, system=None, code=None, **kwargs_):
        super(codingTypeSub, self).__init__(system, code,  **kwargs_)
supermod.codingType.subclass = codingTypeSub
# end class codingTypeSub


class systemType4Sub(supermod.systemType4):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(systemType4Sub, self).__init__(value, valueOf_,  **kwargs_)
supermod.systemType4.subclass = systemType4Sub
# end class systemType4Sub


class codeTypeSub(supermod.codeType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(codeTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.codeType.subclass = codeTypeSub
# end class codeTypeSub


class immutableTypeSub(supermod.immutableType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(immutableTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.immutableType.subclass = immutableTypeSub
# end class immutableTypeSub


class copyrightTypeSub(supermod.copyrightType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(copyrightTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.copyrightType.subclass = copyrightTypeSub
# end class copyrightTypeSub


class composeTypeSub(supermod.composeType):
    def __init__(self, include=None, **kwargs_):
        super(composeTypeSub, self).__init__(include,  **kwargs_)
supermod.composeType.subclass = composeTypeSub
# end class composeTypeSub


class includeTypeSub(supermod.includeType):
    def __init__(self, system=None, concept=None, **kwargs_):
        super(includeTypeSub, self).__init__(system, concept,  **kwargs_)
supermod.includeType.subclass = includeTypeSub
# end class includeTypeSub


class systemType5Sub(supermod.systemType5):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(systemType5Sub, self).__init__(value, valueOf_,  **kwargs_)
supermod.systemType5.subclass = systemType5Sub
# end class systemType5Sub


class conceptTypeSub(supermod.conceptType):
    def __init__(self, code=None, display=None, designation=None, **kwargs_):
        super(conceptTypeSub, self).__init__(code, display, designation,  **kwargs_)
supermod.conceptType.subclass = conceptTypeSub
# end class conceptTypeSub


class codeType6Sub(supermod.codeType6):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(codeType6Sub, self).__init__(value, valueOf_,  **kwargs_)
supermod.codeType6.subclass = codeType6Sub
# end class codeType6Sub


class displayTypeSub(supermod.displayType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(displayTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.displayType.subclass = displayTypeSub
# end class displayTypeSub


class designationTypeSub(supermod.designationType):
    def __init__(self, language=None, value=None, **kwargs_):
        super(designationTypeSub, self).__init__(language, value,  **kwargs_)
supermod.designationType.subclass = designationTypeSub
# end class designationTypeSub


class languageTypeSub(supermod.languageType):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(languageTypeSub, self).__init__(value, valueOf_,  **kwargs_)
supermod.languageType.subclass = languageTypeSub
# end class languageTypeSub


class valueType7Sub(supermod.valueType7):
    def __init__(self, value=None, valueOf_=None, **kwargs_):
        super(valueType7Sub, self).__init__(value, valueOf_,  **kwargs_)
supermod.valueType7.subclass = valueType7Sub
# end class valueType7Sub


def get_root_tag(node):
    tag = supermod.Tag_pattern_.match(node.tag).groups()[-1]
    rootClass = None
    rootClass = supermod.GDSClassesMapping.get(tag)
    if rootClass is None and hasattr(supermod, tag):
        rootClass = getattr(supermod, tag)
    return tag, rootClass


def parse(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'ValueSet'
        rootClass = supermod.ValueSet
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='',
            pretty_print=True)
    return rootObj


def parseEtree(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'ValueSet'
        rootClass = supermod.ValueSet
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    mapping = {}
    rootElement = rootObj.to_etree(None, name_=rootTag, mapping_=mapping)
    reverse_mapping = rootObj.gds_reverse_node_mapping(mapping)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        content = etree_.tostring(
            rootElement, pretty_print=True,
            xml_declaration=True, encoding="utf-8")
        sys.stdout.write(content)
        sys.stdout.write('\n')
    return rootObj, rootElement, mapping, reverse_mapping


def parseString(inString, silence=False):
    if sys.version_info.major == 2:
        from StringIO import StringIO
    else:
        from io import BytesIO as StringIO
    parser = None
    rootNode= parsexmlstring_(inString, parser)
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'ValueSet'
        rootClass = supermod.ValueSet
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='')
    return rootObj


def parseLiteral(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'ValueSet'
        rootClass = supermod.ValueSet
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('#from ??? import *\n\n')
        sys.stdout.write('import ??? as model_\n\n')
        sys.stdout.write('rootObj = model_.rootClass(\n')
        rootObj.exportLiteral(sys.stdout, 0, name_=rootTag)
        sys.stdout.write(')\n')
    return rootObj


USAGE_TEXT = """
Usage: python ???.py <infilename>
"""


def usage():
    print(USAGE_TEXT)
    sys.exit(1)


def main():
    args = sys.argv[1:]
    if len(args) != 1:
        usage()
    infilename = args[0]
    parse(infilename)


if __name__ == '__main__':
    #import pdb; pdb.set_trace()
    main()
