#!/usr/bin/env python

#
# Generated Fri Aug 11 11:08:08 2023 by generateDS.py version 2.42.2.
# Python 3.10.12 (main, Jun 11 2023, 05:26:28) [GCC 11.4.0]
#
# Command line options:
#   ('-o', 'svsEPD.py')
#   ('-s', 'svsEPDsubs.py')
#
# Command line arguments:
#   EPDsvs.xsd
#
# Command line:
#   generateDS.py -o "svsEPD.py" -s "svsEPDsubs.py" EPDsvs.xsd
#
# Current working directory (os.getcwd()):
#   generateDS-2.42.2
#

from fileinput import filename
import os
import sys
import re
from lxml import etree as etree_

import svsEPD as supermod
import prop_csv_and_masterEPD as pcam

class XMLEscapeSpecialChar:

    def __init__(self, xml, escape):
        self.xml = xml
        self.escape = escape

xml_escape_special_char = [XMLEscapeSpecialChar('>', 'groesser'),
                           XMLEscapeSpecialChar('<', 'kleiner')]

def parsexml_(infile, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        parser = etree_.ETCompatXMLParser()
    try:
        if isinstance(infile, os.PathLike):
            infile = os.path.join(infile)
    except AttributeError:
        pass
    doc = etree_.parse(infile, parser=parser, **kwargs)
    return doc

def parsexmlstring_(instring, parser=None, **kwargs):
    if parser is None:
        # Use the lxml ElementTree compatible parser so that, e.g.,
        #   we ignore comments.
        try:
            parser = etree_.ETCompatXMLParser()
        except AttributeError:
            # fallback to xml.etree
            parser = etree_.XMLParser()
    element = etree_.fromstring(instring, parser=parser, **kwargs)
    return element

#
# Globals
#

ExternalEncoding = ''
SaveElementTreeNode = True

#
# Data representation classes
#

class SVSextEPD1Sub(supermod.SVSextEPD2, pcam.PropCsvAndMaster):
    def __init__(self, id=None, displayName=None, version=None, Source=None, Purpose=None, Status=None, Type=None, EffectiveDate=None, CreationDate=None, ConceptList=None, **kwargs_):
        super(SVSextEPD1Sub, self).__init__(id, displayName, version, Source, Purpose, Status, Type, EffectiveDate, CreationDate, ConceptList,  **kwargs_)

# just for calling the outer method
    def parse(inFilename):
        return parse(inFilename)

    def get_resource(self):
        if hasattr(self,"resource") and (res := self.resource):
            return res
        elif hasattr(self,"filename") and (fn := self.filename):
            if os.path.basename(fn).startswith("CodeSystem-"):
                return pcam.Resource.CodeSystem.value
            elif os.path.basename(fn).startswith("ValueSet-"):
                return pcam.Resource.ValueSet.value
        else:
            return pcam.Resource.ValueSet.value

    def get_id(self):
        #return self.get_id()
        return super(SVSextEPD1Sub,self).get_id()

    def get_version(self):
        return super(SVSextEPD1Sub,self).get_version()

    def get_name(self):
        return super(SVSextEPD1Sub,self).get_displayName()
        #return self.get_name()
        #return (re.sub(r'[^A-Za-z0-9\-]+', '', super(SVSextEPD1Sub,self).get_displayName().replace(" ","-").replace("_","-").replace("--","-").replace("---","-").replace("--","-"))).strip("-").lower()

    def get_status(self):
        #return super(SVSextEPD1Sub,self).get_status
        # das noch einmal anschauen
        return pcam.Status.active.value

    # def get_content(self):
    #     if hasattr(self,"content") and (tmp := self.content):
    #         return tmp
    #     else:
    #         return pcam.Content.complete.value
#für FHIR
    def get_implicitrules(self):
        return None
#für FHIR
    def get_language(self):
        return None

    # def get_text(self):
    #     return None

    # def get_contained(self):
    #     return None

    # def get_extension(self):
    #     return None

    # def get_modifierextension(self):
    #     return None
# für FHIR
    def get_identifier(self):
        if hasattr(self,"identifier"):
            return self.identifier
        identifiers = []
        if tempID := super().get_id():
            identifiers.append(tempID)
        return identifiers
# für FHIR
    def get_experimental(self):
        return None
# für FHIR
    def get_date(self):
        return str(self.get_EffectiveDate())

    # def get_publisher(self):
    #     if (tmp := self.get_verantw_Org()) and tmp != "":
    #         return tmp
    #     return None

    # def get_contact(self):
    #     tempConDetS = []
    #     if (website := self.get_website()) and not website == "":
    #         oneConDet = pcam.ContactDetail()
    #         oneConPoi = pcam.ContactDetail.ContactPoint()
    #         oneConPoi.value = website
    #         oneConPoi.system = pcam.ContactDetail.ContactPoint.ContactPointSystem.url
    #         oneConDet.telecom.append(oneConPoi)
    #         tempConDetS.append(oneConDet)
    #     return tempConDetS

    # def get_usecontext(self):
    #     return None

    # def get_jurisdiction(self):
    #     return None

    def get_purpose(self):
        return super(SVSextEPD1Sub,self).get_Purpose
# für FHIR
    def get_copyright(self):
        return None
# für FHIR
    def get_immutable(self):
        return None
# für FHIR
    def get_lockeddate(self):
        return None
# für FHIR
    def get_inactive(self):
        return None
    
# für FHIR
    def get_url(self):
        if tempUrl := self.globalUrl:
            return tempUrl+self.get_resource()+"/"+self.get_id()
        return None

    # def get_casesensitive(self):
    #     return None

    # def get_valueset(self):
    #     return None

    # def get_hierarchymeaning(self):
    #     return None

    # def get_compositional(self):
    #     return None

    # def get_versionneeded(self):
    #     return None

    def get_supplements(self):
        tmpSupplUrl = ""
        if tempUrl := self.globalUrl:
            tmpSupplUrl += tempUrl
        if hasattr(self,"supplements") and (tmp := self.supplements) and tmp != "":
            return tmpSupplUrl+tmp
        return None

    # def get_count(self):
    #     return len(self.get_concept())

    # def get_filter(self):
    #     return None

    # def get_testConcept(self):
    #     return self.concept

    def get_concept(self):
        if hasattr(self, "Concept") and self.concept:
            return self.concept
        else:
            retArr = []
            #theMemberParent = [None]*9
            #sort with the orderNumber or sort after code
            sortedConceptList = self.get_ConceptList().get_Concept()
            # if sortedConceptList[0].get_orderNumber() is not None:
            #     sortedConceptList.sort(key=supermod.concept.get_orderNumber)
            #disable sorting of concepts if there is no ordernumber
            # else:
            #    sortedConceptList.sort(key=supermod.concept.get_code)
            #iterate though the list
            if self.get_resource() == "ValueSet":
                for oneCon in sortedConceptList:
                    theConcept = pcam.VSConcept()
                    theConcept.system = oneCon.get_codeSystem()
                    theConcept.code= oneCon.get_code()
                    theConcept.display = oneCon.get_displayName()
                    
                    # if tmp := oneCon.get_parentCodeSystemName():
                    #     if tmp.startswith('http://') or tmp.startswith('https://'):
                    #         theConcept.system = tmp
                    #     else:
                    #         theConcept.system = self.globalUrl+"CodeSystem-"+ (re.sub(r'[^A-Za-z0-9\-]+', '', oneCon.get_parentCodeSystemName().replace(" ","-").replace("_","-").replace("--","-").replace("---","-").replace("--","-"))).strip("-").lower()
                    # elif tmp:= oneCon.get_codeSystem():
                    #     theConcept.system = self.gainCanonicalFromOid(tmp)
                    # else:
                    #     sys.exit('Neither OID nor name of the parent CodeSytem is present. Cannot process ValueSet.')
                    #print("test")
                    # theConcept.code = oneCon.get_code()
                    # print(pcam.VSConcept.get_code)
                    # theConcept.display = oneCon.get_displayName()
                    # theConcept.system = oneCon.get_codeSystem()
                    retArr.append(theConcept)
            self.concept = retArr
            return retArr

            #         if oneCon.get_type() == "A":
            #             theConcept.abstract = True
            #         if oneCon.get_type() == "D":
            #             theConcept.inactive = True
            #             self.inactive = True
            #         if (tmpDeutsch := oneCon.get_deutsch()) and tmpDeutsch.strip() != "" and tmpDeutsch != oneCon.get_displayName():
            #             theDesig = pcam.ConceptDesignation()
            #             theDesig.language = "de-AT"
            #             theDesig.value = tmpDeutsch
            #             theConcept.designation.append(theDesig)
            #         if (tmp := oneCon.get_concept_beschreibung()) and tmp.strip() != "" and tmp != oneCon.get_displayName() and tmp != tmpDeutsch and tmp != tmpDisAlt:
            #             theDesig = pcam.ConceptDesignation()
            #             theDesig.use = pcam.Coding(self.globalUrl+"CodeSystem-"+"austrian-designation-use^^concept_beschreibung^concept_beschreibung^")
            #             theDesig.value = tmp
            #             theConcept.designation.append(theDesig)
            #         if (tmp := oneCon.get_relationships()) and tmp.strip() != "":
            #             theDesig = pcam.ConceptDesignation()
            #             theDesig.use = pcam.Coding(self.globalUrl+"CodeSystem-"+"austrian-designation-use^^relationships^relationships^")
            #             theDesig.value = tmp
            #             theConcept.designation.append(theDesig)
            #         if (tmpDisAlt := oneCon.get_displayNameAlt()) and tmpDisAlt.strip() != "" and tmpDisAlt != oneCon.get_displayName() and tmpDisAlt != tmpDeutsch:
            #             theDesig = pcam.ConceptDesignation()
            #             theDesig.use = pcam.Coding(self.globalUrl+"CodeSystem-"+"austrian-designation-use^^alt^alt^")
            #             theDesig.value = tmpDisAlt
            #             theConcept.designation.append(theDesig)
            #         if (tmp := oneCon.get_einheit_codiert()) and tmp.strip() != "":
            #             theDesig = pcam.ConceptDesignation()
            #             theDesig.use = pcam.Coding(self.globalUrl+"CodeSystem-"+"austrian-designation-use^^einheit_codiert^einheit_codiert^")
            #             theDesig.value = tmp
            #             theConcept.designation.append(theDesig)
            #         if (tmp := oneCon.get_einheit_print()) and tmp.strip() != "":
            #             theDesig = pcam.ConceptDesignation()
            #             theDesig.use = pcam.Coding(self.globalUrl+"CodeSystem-"+"austrian-designation-use^^einheit_print^einheit_print^")
            #             theDesig.value = tmp
            #             theConcept.designation.append(theDesig)
            #         if (tmp := oneCon.get_hinweise()) and tmp.strip() != "":
            #             theDesig = pcam.ConceptDesignation()
            #             theDesig.use = pcam.Coding(self.globalUrl+"CodeSystem-"+"austrian-designation-use^^hinweise^hinweise^")
            #             theDesig.value = tmp
            #             theConcept.designation.append(theDesig)

            #         if 0 <= (tmpLevel := int(oneCon.get_level().strip())):
            #             theConcept.level = tmpLevel
            #             theMemberParent[tmpLevel] = theConcept
            #             if tmpLevel > 0:
            #                 parent = theMemberParent[tmpLevel-1]
            #                 # setting theConcept as child of the parent
            #                 parent.member.append(theConcept)
            #                 # setting the parent of theConcept
            #                 theConcept.parent = parent


            #         # read anyAttribute if version 2
            #         if self.versionSVSextELGA == 2:
            #             for oneAttrKey in oneCon.get_anyAttributes_():
            #                 # read the codesytem version if existent
            #                 if oneAttrKey == 'codeSystemVersion':
            #                     theConcept.version = oneCon.get_anyAttributes_()[oneAttrKey]
            #                 else:
            #                     theDesig = pcam.ConceptDesignation()

            #                     code = oneAttrKey
            #                     for one_xml_escape_special_char in xml_escape_special_char:
            #                         code = code.replace(one_xml_escape_special_char.escape, one_xml_escape_special_char.xml)

            #                     theDesig.use = pcam.Coding(self.globalUrl+"CodeSystem-"+"austrian-designation-use^^"+code+"^"+code)
            #                     theDesig.value = oneCon.get_anyAttributes_()[oneAttrKey]
            #                     theConcept.designation.append(theDesig)

            #         retArr.append(theConcept)

            # elif self.get_resource() == "CodeSystem":
            #     for oneCon in sortedConceptList:
            #         theConcept = pcam.CSConcept()
            #         theConcept.code = oneCon.get_code()
            #         theConcept.display = oneCon.get_displayName()
            #         retiredAlreadySet = False

            #         if 0 <= (tmpLevel := int(oneCon.get_level().strip())):
            #             theMemberParent[tmpLevel] = theConcept
            #             if tmpLevel > 0:
            #                 theProperty = pcam.CSConcept.ConceptProperty()
            #                 theProperty.code = "child"
            #                 theProperty.value = oneCon.get_code()
            #                 theProperty.type = pcam.PropertyCodeType.code
            #                 theMemberParent[tmpLevel-1].property.append(theProperty)

            #                 theProperty = pcam.CSConcept.ConceptProperty()
            #                 theProperty.code = "parent"
            #                 theProperty.value = theMemberParent[tmpLevel-1].code
            #                 theProperty.type = pcam.PropertyCodeType.code
            #                 theConcept.property.append(theProperty)

            #         if (theConcept.display.upper().startswith("DEPRECATED") and not retiredAlreadySet):
            #             self.set_statusToRetired(theConcept)
            #             retiredAlreadySet = True
            #         if (oneCon.get_type() == "D" and not retiredAlreadySet):
            #             self.set_statusToRetired(theConcept)
            #             retiredAlreadySet = True
            #         if oneCon.get_type() == "A":
            #             theProp = pcam.CSConcept.ConceptProperty()
            #             theProp.code = "notSelectable"
            #             theProp.value = True
            #             theProp.type = pcam.PropertyCodeType.boolean
            #             theConcept.property.append(theProp)

            #         if (tmpDeutsch := oneCon.get_deutsch()) and tmpDeutsch.strip() != "" and tmpDeutsch != oneCon.get_displayName():
            #             theDesig = pcam.ConceptDesignation()
            #             theDesig.language = "de-AT"
            #             theDesig.value = tmpDeutsch
            #             theConcept.designation.append(theDesig)
            #         if (tmpDisAlt := oneCon.get_displayNameAlt()) and tmpDisAlt.strip() != "" and tmpDisAlt != oneCon.get_displayName() and tmpDisAlt != tmpDeutsch:
            #             theProp = pcam.CSConcept.ConceptProperty()
            #             theProp.code = "displayNameAlt"
            #             theProp.value = tmpDisAlt
            #             theProp.type = pcam.PropertyCodeType.string
            #             theConcept.property.append(theProp)
            #         if (tmp := oneCon.get_concept_beschreibung()) and tmp.strip() != "" and tmp != oneCon.get_displayName() and tmp != tmpDeutsch and tmp != tmpDisAlt:
            #             theConcept.definition = tmp
            #             #theProp = pcam.CSConcept.ConceptProperty()
            #             #theProp.code = "concept_beschreibung"
            #             #theProp.value = tmp
            #             #theProp.type = pcam.PropertyCodeType.string
            #             #theConcept.property.append(theProp)
            #         if (tmp := oneCon.get_einheit_codiert()) and tmp.strip() != "":
            #             theProp = pcam.CSConcept.ConceptProperty()
            #             theProp.code = "einheit_codiert"
            #             theProp.value = tmp
            #             theProp.type = pcam.PropertyCodeType.string
            #             theConcept.property.append(theProp)
            #         if (tmp := oneCon.get_einheit_print()) and tmp.strip() != "":
            #             theProp = pcam.CSConcept.ConceptProperty()
            #             theProp.code = "einheit_print"
            #             theProp.value = tmp
            #             theProp.type = pcam.PropertyCodeType.string
            #             theConcept.property.append(theProp)
            #         if (tmp := oneCon.get_relationships()) and tmp.strip() != "":
            #             theProp = pcam.CSConcept.ConceptProperty()
            #             theProp.code = "Relationships"
            #             theProp.value = tmp
            #             theProp.type = pcam.PropertyCodeType.string
            #             theConcept.property.append(theProp)
            #         if (tmp := oneCon.get_hinweise()) and tmp.strip() != "":
            #             if (tmp.upper().startswith("DEPRECATED") or tmp.upper().startswith("INAKTIV")) and not retiredAlreadySet:
            #                 self.set_statusToRetired(theConcept)
            #                 retiredAlreadySet = True
            #             else:
            #                 theProp = pcam.CSConcept.ConceptProperty()
            #                 theProp.code = "hints"
            #                 theProp.value = tmp
            #                 theProp.type = pcam.PropertyCodeType.string
            #                 theConcept.property.append(theProp)

            #         # read anyAttribute if version 2
            #         if self.versionSVSextELGA == 2:
            #             for oneAttrKey in oneCon.get_anyAttributes_():
            #                 theProp = pcam.CSConcept.ConceptProperty()

            #                 code = oneAttrKey
            #                 for one_xml_escape_special_char in xml_escape_special_char:
            #                     code = code.replace(one_xml_escape_special_char.escape, one_xml_escape_special_char.xml)
            #                 theProp.code = code

            #                 theProp.value = oneCon.get_anyAttributes_()[oneAttrKey]
            #                 theProp.type = pcam.PropertyCodeType.string
            #                 theConcept.property.append(theProp)

            #             retArr.append(theConcept)
            # self.concept = retArr
            # return retArr

    def exporto(self, outfile, outputClass, argsLang=None, incHierarchyExt4VS=False, versionSVSextEPD=1):
        # beschreibung = description = ""
        # if (tmp := self.get_description()) and tmp.strip() != "":
        #     desc_splitted_by_star_star = tmp.split('**')
        #     headers = False
        #     if "Description:" in desc_splitted_by_star_star:
        #         description = desc_splitted_by_star_star[desc_splitted_by_star_star.index("Description:")+1].strip()
        #         headers = True
        #     if "Beschreibung:" in desc_splitted_by_star_star:
        #         beschreibung = desc_splitted_by_star_star[desc_splitted_by_star_star.index("Beschreibung:")+1].strip()
        #         headers = True
        #     if not headers:
        #         beschreibung = tmp.strip()

        
        EffectiveDate = self.get_date()
        CreationDate = self.get_date()
        #"2022-09-09"
        #or date.today() # TODO should here be the today date?
        #gueltigkeitsbereich = "empfohlen"

        # if (identifier := self.get_identifier()) and (tmp := identifier[0]) and (re.match('([0-9]+\.)+[0-9]+',tmp)):
        #     id = tmp
        # else:
        #     id = self.gainOidFromName(self.get_resource()+"-"+self.get_id())
        # statusCode = "1"
        # verantw_Org = self.get_copyright or ""
        
        version = self.get_version()
        id = self.get_id()
        displayName = self.get_name()
        Status =self.get_status()
        Purpose=self.get_purpose()
        Source=None
        #Purpose = self.get_purpose()
        #Source=self.get_source()

        # foundIt = False
        # for oneConcept in self.get_contact():
        #     if (tmp := oneConcept.telecom) and tmp[0].system == pcam.ContactDetail.ContactPoint.ContactPointSystem.url:
        #         website = oneConcept.telecom[0].value
        #         foundIt = True
        # if not foundIt:
        #     website = ""

        concepts = []
        #concepts.append(ConceptTypeSub(pcam.VSConcept.code, pcam.VSConcept.system, pcam.VSConcept.display))
        #concepts.append(ConceptTypeSub("123", "1234567", "testDisplay"))
        for oneConcept in self.get_concept():
            concepts.append(ConceptTypeSub(oneConcept.code, oneConcept.system, oneConcept.display))
            #concepts.append(ConceptTypeSub("", "", ""))
        ConceptList = ConceptListTypeSub("en", concepts)
        Status =self.get_status()
        #Type =self.get_type()
        Type=None
        svsExtEPD2 = SVSextEPD1Sub(id, displayName, version, Source, Purpose, Status, Type, EffectiveDate, CreationDate, ConceptList) 
                                   #Source, Purpose, Status, Type, EffectiveDate, CreationDate, ConceptList, versionSVSextEPD=1)
        print(concepts.__len__())
        print(ConceptList)
        svsExtEPD2.export(outfile, 0)
supermod.SVSextEPD2.subclass = SVSextEPD1Sub



# end class ValueSetSub

#TESTING
class SVSextEPD2Sub(SVSextEPD1Sub):
    def __init__(self, id=None, displayName=None, version=None, Source=None, Purpose=None, Status=None, Type=None, EffectiveDate=None, CreationDate=None, ConceptList=None, **kwargs_):
        SVSextEPD1Sub.__init__(self, id, displayName, version, Source, Purpose, Status, Type, EffectiveDate, CreationDate, ConceptList, **kwargs_)

    def exporto(self, outfile, outputClass, argsLang=None, incHierarchyExt4VS=False, versionSVSextEPD=2):
        return SVSextEPD1Sub.exporto(self, outfile, outputClass, argsLang, incHierarchyExt4VS, versionSVSextEPD)
supermod.SVSextEPD2.subclass = SVSextEPD2Sub

class ConceptListTypeSub(supermod.ConceptListType):
    def __init__(self, lang=None, Concept=None, **kwargs_):
        super(ConceptListTypeSub, self).__init__(lang, Concept,  **kwargs_)
supermod.ConceptListType.subclass = ConceptListTypeSub
# end class ConceptListTypeSub


class ConceptTypeSub(supermod.ConceptType):
    def __init__(self, code=None, codeSystem=None, displayName=None, valueOf_=None, **kwargs_):
        super(ConceptTypeSub, self).__init__(code, codeSystem, displayName, valueOf_,  **kwargs_)
supermod.ConceptType.subclass = ConceptTypeSub
# end class ConceptTypeSub


def get_root_tag(node):
    tag = supermod.Tag_pattern_.match(node.tag).groups()[-1]
    rootClass = None
    rootClass = supermod.GDSClassesMapping.get(tag)
    if rootClass is None and hasattr(supermod, tag):
        rootClass = getattr(supermod, tag)
    return tag, rootClass


def parse(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'ValueSet'
        rootClass = supermod.SVSextEPD2
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='',
            pretty_print=True)
    return rootObj


def parseEtree(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'ValueSet'
        rootClass = supermod.SVSextEPD2
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    mapping = {}
    rootElement = rootObj.to_etree(None, name_=rootTag, mapping_=mapping)
    reverse_mapping = rootObj.gds_reverse_node_mapping(mapping)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        content = etree_.tostring(
            rootElement, pretty_print=True,
            xml_declaration=True, encoding="utf-8")
        sys.stdout.write(content)
        sys.stdout.write('\n')
    return rootObj, rootElement, mapping, reverse_mapping


def parseString(inString, silence=False):
    if sys.version_info.major == 2:
        from StringIO import StringIO
    else:
        from io import BytesIO as StringIO
    parser = None
    rootNode= parsexmlstring_(inString, parser)
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'ValueSet'
        rootClass = supermod.SVSextEPD2
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        rootNode = None
    if not silence:
        sys.stdout.write('<?xml version="1.0" ?>\n')
        rootObj.export(
            sys.stdout, 0, name_=rootTag,
            namespacedef_='')
    return rootObj


def parseLiteral(inFilename, silence=False):
    parser = None
    doc = parsexml_(inFilename, parser)
    rootNode = doc.getroot()
    rootTag, rootClass = get_root_tag(rootNode)
    if rootClass is None:
        rootTag = 'ValueSet'
        rootClass = supermod.SVSextEPD2
    rootObj = rootClass.factory()
    rootObj.build(rootNode)
    # Enable Python to collect the space used by the DOM.
    if not SaveElementTreeNode:
        doc = None
        rootNode = None
    if not silence:
        sys.stdout.write('#from ??? import *\n\n')
        sys.stdout.write('import ??? as model_\n\n')
        sys.stdout.write('rootObj = model_.rootClass(\n')
        rootObj.exportLiteral(sys.stdout, 0, name_=rootTag)
        sys.stdout.write(')\n')
    return rootObj


USAGE_TEXT = """
Usage: python ???.py <infilename>
"""


def usage():
    print(USAGE_TEXT)
    sys.exit(1)


def main():
    args = sys.argv[1:]
    if len(args) != 1:
        usage()
    infilename = args[0]
    parse(infilename)


if __name__ == '__main__':
    #import pdb; pdb.set_trace()
    main()