import enum
from unittest import skip

class FileStyle(enum.Enum):
    propCSV = "propCSV"
    spreadCT = "spreadCT"

class ParentingBy(enum.Enum):
    conceptOrder = "conceptOrder"
    parentAndSystem = "parentAndSystem"

    def set_unchangeable_parenting_by(finalVariable, newValue):
        if finalVariable and finalVariable != newValue:
            import sys
            sys.exit('It is only allowed to use parentByConceptOrder or parent and parentSystem, not both, at the same time.')
        else:
            return newValue

class PropCsvAndMaster():
    # following are use only in malac-ct
    parenting_by = None

    def __init__(self, inFile = None, encoding='cp1252', file_style=FileStyle.propCSV):
        self.resource = Resource
        self.id = None
        self.url = None
        self.type = Type
        self.effectiveDate = None
        self.creationDate = None
        self.version = None
        self.name = None
        self.status = Status
        self.purpose = None
        self.source = None
        self.concept = []
        # ab hier für FHIR
        self.language =None
        self.implicitrules=None
        self.identifier = []
        self.experimental = None
        self.date = None
        self.copyright = None
        self.immutable = None
        self.lockeddate = None
        self.inactive = False

        if inFile is not None:
            try:
                import csv
                if file_style is FileStyle.spreadCT:
                    inFile = inFile.replace(".data.1.spreadct.csv",".meta.1.spreadct.csv")
                    encoding = "utf-8"

                # usally open file with special encoding because of Excel compatibility,
                # see https://stackoverflow.com/questions/6588068/which-encoding-opens-csv-files-correctly-with-excel-on-both-mac-and-windows
                with open(inFile, 'r', encoding=encoding) as csvfile:
                    # creating dictreader object
                    file = csv.reader(csvfile, quotechar='"', delimiter=';')

                    if file_style is FileStyle.propCSV:
                        # iterating over first two rows and append values to empty list
                        metaList = [file.__next__()]+[file.__next__()]
                        # iterating over a empty row
                        file.__next__()
                    elif file_style is FileStyle.spreadCT:
                        metaList = []
                        for metaLine in file:
                            metaList.append(metaLine)
                        metaList = list(zip(*metaList)) #transpose the list

                    #initializing the header/metadata
                    i = -1
                    for metaObj in metaList[0]:
                        i += 1
                        # change all ';' to ','
                        #metaList[1][i].replace(';',',')
                        metaList[1][i].strip("\"")

                        if 'resource' == metaObj: self.resource = Resource(metaList[1][i])
                        if 'id' == metaObj: self.id = metaList[1][i]
                        if 'url' == metaObj: self.url = metaList[1][i]
                        if 'type' == metaObj: self.type = Type(metaList[1][i])
                        if 'effectiveDate' == metaObj: self.effectiveDate = metaList[1][i]
                        if 'creationDate' == metaObj: self.creationDate = metaList[1][i]
                        if 'version' == metaObj: self.version = metaList[1][i]
                        if 'name' == metaObj: self.name = metaList[1][i]
                        if 'status' == metaObj: self.status = Status(metaList[1][i])
                        if 'source' == metaObj: self.source = metaList[1][i]
                        if 'valueSet' == metaObj: self.valueset = metaList[1][i]

                    if file_style is FileStyle.spreadCT:
                        csvfile.close()
                        csvfile = open(inFile.replace(".meta.1.spreadct.csv",".data.1.spreadct.csv"), 'r', encoding=encoding)
                        file = csv.reader(csvfile, quotechar='"', delimiter=';')

                    # iterating over the next row with headings for concepts
                    headingList = file.__next__()

                    #initializing the body/content
                    for row in file:
                        if self.resource == self.resource.CodeSystem:
                            tempConcept = CSConcept()
                        elif self.resource == self.resource.ValueSet:
                            tempConcept = VSConcept()

                        # reading the content of the row
                        for i in range(0, len(row)):
                            if row[i].strip() != "":
                                row[i].strip("\"").strip("\'")
                            
                            # folgender Block auskommentiert, da aktuell nur ValueSets
                            # if self.resource == self.resource.CodeSystem:
                            #     if 'code' == headingList[i]: tempConcept.code = row[i]
                            #     if 'display' == headingList[i]: tempConcept.display = row[i]
                            #     if 'definition' == headingList[i]: tempConcept.definition = row[i]
                            #     if 'designation' == headingList[i]:
                            #         tempConcept.designation.append(ConceptDesignation(row[i]))
                            #     if 'property' == headingList[i]:
                            #         tempConcept.property.append(CSConcept.ConceptProperty(row[i]))
                                if self.resource == self.resource.ValueSet:
                                    if 'system' == headingList[i]: tempConcept.system = row[i]
                                    # Version haben wir auf Code Ebene nicht, dafür haben wir ein von bis datum -> To Do
                                    #if 'version' == headingList[i]: tempConcept.version = row[i]
                                    if 'code' == headingList[i]: tempConcept.code = row[i]
                                    if 'display' == headingList[i]: tempConcept.display = row[i]
                                    # Haben wir Hierarchie mit Parents?
                                    #if 'parent' == headingList[i]: tempConcept.parent = row[i]
                                    #if 'parentSystem' == headingList[i]: tempConcept.parentSystem = row[i]
                                    # Designation noch anschauen für die Sprachen -> To do
                                    if 'designation' == headingList[i]:
                                        tempConcept.designation.append(ConceptDesignation(row[i]))

                        # check for some invalid combinations
                        if self.resource == self.resource.ValueSet:
                            # both, the parent and the parentSystem, have to exist together or do not, aditionally for parentSystem the attr has to be existent
                            if bool(tempConcept.parent) != (hasattr(tempConcept,"parentSystem") and bool(tempConcept.parentSystem)):
                                import sys
                                sys.exit('Parent code and system should always be stated together. See concept with code: ' + tempConcept.code)
                        self.concept.append(tempConcept)

                    # fill members of parents
                    if self.resource == self.resource.ValueSet:
                        concept_dict = self.buildConceptDictWithAttr('code', 'system')

                        for one_concept in self.concept:
                            if one_concept.parent and one_concept.parentSystem:
                                parent = concept_dict[one_concept.parent+one_concept.parentSystem]
                                # setting one_concept as child of the parent
                                parent.member.append(one_concept)
                                # setting the parent of one_concept
                                one_concept.parent = parent
                                # setting the hierarchy level
                                one_concept.level = self.retrieve_level_of_VS_concept(concept_dict, one_concept)
                    elif self.resource == self.resource.CodeSystem:
                        concept_dict = self.buildConceptDictWithAttr('code')

                        for one_concept in self.concept:
                            self.add_children_to_CS_concept(concept_dict, one_concept)

            except UnicodeDecodeError:
                print(" (because of some DecodeError, this terminology will be read again in utf-8 instead of cp1252)")
                return PropCsvAndMaster.__init__(self, inFile, encoding='utf-8')



    def retrieve_level_of_VS_concept(self, concept):
        if tmp_parent := coept.parent:
            level = self.retrieve_level_of_VS_concept(tmp_parent)
            return level + 1
        else:
            return 0

    def retrieve_level_of_CS_concept(self, concept_dict, concept):
        if hasattr(concept,'property') and concept.property:
            for oneProperty in concept.property:
                if oneProperty.code == "parent":
                    level = self.retrieve_level_of_CS_concept(concept_dict, concept_dict[oneProperty.value])
                    return level + 1
        return 0

    def add_children_to_CS_concept(self, concept_dict, concept):
        if hasattr(concept,'property') and concept.property:
            for oneProperty in concept.property:
                if oneProperty.code == "parent":
                    if not hasattr(concept_dict[oneProperty.value], "propChildDict"):
                        # there is no managing of the now added props in the dict, but a code cant exists twice in a CS, so that should be good
                        concept_dict[oneProperty.value].propChildDict = self.build_property_dict_on_code_with_value_as_key(concept_dict[oneProperty.value],"child")
                    if concept.code not in concept_dict[oneProperty.value].propChildDict.keys():
                        the_property_child = CSConcept.ConceptProperty()
                        the_property_child.code = "child"
                        the_property_child.value = concept.code
                        the_property_child.type = PropertyCodeType.code
                        concept_dict[oneProperty.value].property.append(the_property_child)

    def parse(inFilename):
        return PropCsvAndMaster(inFilename)

    def get_resource(self):
        return self.resource.value if hasattr(self,"resource") and hasattr(self.resource,"value") else None
    
    def get_id(self):
        return self.id
    
    def get_url(self):
        return self.url
    
    def get_type(self):
        return self.type.value
    
    def get_effecticveDate(self):
        return self.EffectiveDate
    
    def get_creationDate(self):
        return self.CreationDate
    
    def get_version(self):
        return self.version
    
    def get_name(self):
        return self.name

    def get_status(self):
        return self.status.value
    
    def get_purpose(self):
        return self.purpose
    
    def get_source(self):
        return self.source

    def get_concept(self):
        return self.concept
    
    # ab hier für FHIR
    def get_language(self):
        return self.language
    
    def get_implicitrules(self):
        return self.implicitrules
    
    def get_identifier(self):
        return self.identifier

    def get_experimental(self):
        return self.experimental


    def get_date(self):
        return self.date
    
    def get_copyright(self):
        return self.copyright
    
    def get_immutable(self):
        return self.immutable
    
    def get_lockeddate(self):
        return self.lockeddate

    def get_inactive(self):
        return self.inactive

    # def set_statusToRetired(self, theCSConcept):
    #     theProp = CSConcept.ConceptProperty()
    #     theProp.code = "status"
    #     theProp.value = "retired"
    #     theProp.type = PropertyCodeType.code
    #     theCSConcept.property.append(theProp)

    def appendAdditionalConceptProperties(self, retArr):
        # look into the concepts and add missing properties
        for oneConcept in self.get_concept():
            if hasattr(oneConcept,"property"):
                for oneCProperty in oneConcept.property:
                    foundFullMatch = False
                    #foundTypeMismatch = False # if needed in the future
                    for oneProperty in retArr:
                        if oneCProperty.code == oneProperty.code and oneCProperty.type == oneProperty.type:
                            foundFullMatch = True
                        #elif oneCProperty.code == oneProperty.code and oneCProperty.type != oneProperty.type:
                        #    foundTypeMismatch = True
                    if not foundFullMatch:# and not foundTypeMismatch:
                        retArr.append(Property(code=oneCProperty.code,type=oneCProperty.type))
                    #elif not foundFullMatch and foundTypeMismatch:
                    #    retArr.append(Property(code=oneCProperty.code,type=oneCProperty.type))
        return retArr

    def appendMetaDataTo(self, csvFilename):
        terminology_metadata_csv_columns = ['name', 'canonical', 'oid', 'version', 'id', 'type']
        # if file is new, add headers
        try:
            f = open(csvFilename)
            f.close()
        except IOError:
            with open(csvFilename, 'a', encoding="utf8") as csvfile:
                csvfile.write(','.join(terminology_metadata_csv_columns)+'\n')

        # file is existent open and read into a dict with the first column as key
        import csv
        reader = csv.reader(open(csvFilename))
        mydict = {rows[1]:rows for rows in reader}

        # change the one row or add the row to the dictionary
        mydict[self.get_url()] = [self.get_resource() + "-" + self.get_id(), self.get_url(), (tmp[0] if (tmp := self.get_identifier()) else ""), self.get_version(), self.get_id(), self.get_resource()]

        # sort the dict
        import collections
        mydict = collections.OrderedDict(sorted(mydict.items()))
        mydict.move_to_end("canonical",False)

        # wirte the whole dict
        with open(csvFilename, 'w', newline='', encoding='utf-8') as outfile:
            for line in mydict.values():
                outfile.write(','.join(line)+'\n')

    def resolveTerminologyMapping(self, search_column, search_value, target_column, error_message=None, error_value=None):
        # check if the needed argument is given
        if not hasattr(self, "processedTerminologies"):
            import sys
            sys.exit('A outdated csv needs a terminologies list for gaining OIDs, given with "-appndProcTermTo".\n')

        # import the csv when its the first time used
        if type(self.processedTerminologies) is not list:
            import csv
            with open(self.processedTerminologies, 'r', newline='') as file:
                csvFile = csv.DictReader(file, delimiter=',')
                self.processedTerminologies = []
                # displaying the contents of the CSV file
                for csv_line in csvFile:
                    self.processedTerminologies.append(csv_line)

        # search for the Name in the list and return oid
        for line_dict in self.processedTerminologies:
            if line_dict[search_column] == search_value:
                if line_dict[target_column]:
                    return line_dict[target_column]
        if error_message:
            print(error_message)
        return error_value

    # def gainOidFromName(self, name):
    #     error_message = 'Error: For "%s" as CodeSystem-Name or as canonical the CodeSystem OID was not found in the appndProcTermTo list. Some formats, like the outdatedCSV, need a appndProcTermTo list for gaining OIDs of the used CodeSystems, given with "-appndProcTermTo". In this case, the given value will be used instead.\n' % name
    #     return self.resolveTerminologyMapping('name', name, 'oid', error_message, name)

    # def gainOidFromCanonical(self, canonical):
    #     error_message = 'Error: For "%s" as CodeSystem-Name or as canonical the CodeSystem OID was not found in the appndProcTermTo list. Some formats, like the outdatedCSV, need a appndProcTermTo list for gaining OIDs of the used CodeSystems, given with "-appndProcTermTo". In this case, the given value will be used instead.\n' % canonical
    #     return self.resolveTerminologyMapping('canonical', canonical, 'oid', error_message, canonical)

    # def gainCanonicalFromOid(self, oid):
    #     error_message = 'Error: For "%s" as OID the canonical of the CodeSystem was not found in the appndProcTermTo list. Some formats, like FHIR or FSH, need a appndProcTermTo list for gaining canonicals of the used CodeSystems, given with "-appndProcTermTo". In this case, the given value will be used instead.\n' % oid
    #     return self.resolveTerminologyMapping('oid', oid, 'canonical', error_message, oid)

    # def gainVersionFromCanonical(self, canonical):
    #     return self.resolveTerminologyMapping('canonical', canonical, 'version')

    # def gainVersionFromOid(self, oid):
    #     return self.resolveTerminologyMapping('oid', oid, 'version')

    def buildConceptDictWithAttr(self, *stringAttrAsKeys):
        tmpDict = {}
        for oneCon in self.get_concept():
            key = ''
            for stringAttrAsKey in stringAttrAsKeys:
                tmp_pointer = oneCon
                for part in stringAttrAsKey.split("."):
                    tmpPointer = getattr(tmp_pointer,part)
                key += tmpPointer
            tmpDict[key] = oneCon
        return tmpDict

    def build_property_dict_on_code_with_value_as_key(self, concept, propCode):
        tmpDict = {}
        for oneProp in concept.property:
            if oneProp.code == propCode:
                tmpDict[oneProp.value] = oneProp
        return tmpDict

    def sortConceptsExpandStyle(self, conDict=None):
        sortedConcepts = []

        # build up a dict with code as key
        if not conDict:
            conDict = self.buildConceptDictWithAttr("code")

        # sort all the child properties inside the concepts
        for key in conDict:
            if hasattr(conDict[key],"property") and conDict[key].property:
                conDict[key].property.sort(key=CSConcept.ConceptProperty.get_value_if_code_is_child)

        # find all the concepts with no parent, sorted after key
        allLvl0 = []
        for key in sorted(conDict):
            thereIsAParent = False
            for oneProp in conDict[key].property:
                if oneProp.code == "parent":
                    thereIsAParent = True
            if thereIsAParent == False:
                allLvl0.append(key)

        # call the recr for all elements with no parent
        for lvl0 in allLvl0:
            self.storeSelfAndRecrCallChildren(conDict[lvl0], conDict, sortedConcepts)

        # overwrite the concept list with the just sorted one
        self.concept = sortedConcepts

    def storeSelfAndRecrCallChildren(self, oneCon, conDict, sortedCons):
        sortedCons.append(oneCon)
        for oneProp in oneCon.property:
            if oneProp.code == "child":
                if popdCon := conDict.pop(oneProp.value, None): # This will return conDict[key] if key exists in the dictionary, and None otherwise.
                    self.storeSelfAndRecrCallChildren(popdCon, conDict, sortedCons)


    def exporto(self, outfile, outputClass, argsLang=None, incHierarchyExt4VS=False, file_style=FileStyle.propCSV):
            csv_meta = []
            # start list of metaelements
            csv_meta.append(("resource",self.get_resource()))
            csv_meta.append(("id",self.get_id()))
            csv_meta.append(("url",self.get_url()))
            #csv_meta.append(("type",self.get_type()))
            #csv_meta.append(("effectiveDate", self.get_effecticveDate()))
            #csv_meta.append(("creationDate", self.get_creationDate()))
            csv_meta.append(("version",self.get_version()))
            csv_meta.append(("name",self.get_name()))
            csv_meta.append(("status",self.get_status()))
            # Purpose stimmt noch nicht ganz, muss noch einmal angeschaut werden
            #csv_meta.append(("purpose",self.get_Purpose()))
            # Source stimmt noch nicht ganz, muss noch einmal angeschaut werden
            #csv_meta.append(("source",self.get_Source()))
            
            #if self.get_resource() == "CodeSystem": csv_meta.append(("content",self.get_content()))

            #if tmp := self.get_implicitrules(): csv_meta.append(("implicitRules",tmp))
            #if tmp := self.get_language(): csv_meta.append(("language",tmp))
            #if tmp := self.get_text(): csv_meta.append(("text",tmp))
            #if tmp := self.get_contained(): csv_meta.append(("contained",tmp))
            #if tmp := self.get_extension(): csv_meta.append(("extension",tmp))
            #if tmp := self.get_modifierextension(): csv_meta.append(("modifierExtension",tmp))
            # if tmp := self.get_identifier():
            #     for oneIdentifier in tmp:
            #         csv_meta.append(("identifier",oneIdentifier))
            # if tmp := self.get_experimental(): csv_meta.append(("experimental",tmp))
            if tmp := self.get_date(): csv_meta.append(("date",tmp))
            # if tmp := self.get_publisher(): csv_meta.append(("publisher",tmp))
            #if tmp := self.get_contact():
            #    for oneContact in tmp:
            #        csv_meta.append(("contact", str(oneContact)))
            # if tmp := self.get_usecontext():
            #     for oneUseContext in tmp:
            #         csv_meta.append(("useContext",oneUseContext))
            # if tmp := self.get_jurisdiction():
            #     for oneJurisdiction in tmp:
            #         csv_meta.append(("jurisdiction",oneJurisdiction))
            # if tmp := self.get_purpose(): csv_meta.append(("purpose",tmp))
            if tmp := self.get_copyright(): csv_meta.append(("copyright",tmp))
            # if tmp := self.get_immutable(): csv_meta.append(("immutable",tmp))
            # if tmp := self.get_lockeddate(): csv_meta.append(("lockedDate",tmp))
            # if tmp := self.get_inactive(): csv_meta.append(("inactive",tmp))
            # if tmp := self.get_casesensitive(): csv_meta.append(("caseSensitive",tmp))
            # if tmp := self.get_valueset(): csv_meta.append(("valueSet",tmp))
            # if tmp := self.get_hierarchymeaning(): csv_meta.append(("hierarchyMeaning",tmp.value))
            # if tmp := self.get_compositional(): csv_meta.append(("compositional",tmp))
            # if tmp := self.get_versionneeded(): csv_meta.append(("versionNeeded",tmp))
            # if tmp := self.get_supplements(): csv_meta.append(("supplements",tmp))
            # if tmp := self.get_count(): csv_meta.append(("count",tmp))
            # if tmp := self.get_filter():
            #     for oneFilter in tmp:
            #         csv_meta.append(("filter",oneFilter.code+"|"+oneFilter.description+"|"+oneFilter.operator+"|"+oneFilter.value))
            # if tmp := self.get_property():
            #     for oneProperty in tmp:
            #         csv_meta.append(("property",str(oneProperty)))
            # end list of metaelements

            csv_header = []

            #csv_header_designation_count = 0
            csv_list = []


            #csv_meta.append(("Concept",self.get_concept()))
            conceptList = self.get_concept()

                    # check if there are multiple concepts with the same code and system, so that parentingByConceptOrder is used
            if self.get_resource() == 'ValueSet':
                if len(conceptList) > len(self.buildConceptDictWithAttr('code', 'system')):
                    self.parenting_by = ParentingBy.conceptOrder
                else:
                    self.parenting_by = ParentingBy.parentAndSystem
            
            for oneConcept in conceptList:
                csv_line = []
                #for CodeSystems
                if self.get_resource() == 'CodeSystem':
                    #adding values to already existing columns, if value empty then an empty string is added
                    headerDesignationIterator = 0
                    headerPropertyIterator = 0
                    headerExtensionIterator = 0
                    for csv_header_element in csv_header:

                        if csv_header_element == "designation":
                            if headerDesignationIterator < len(oneConcept.designation):
                                csv_line.append(oneConcept.designation[headerDesignationIterator])
                            else:
                                csv_line.append('')
                            headerDesignationIterator += 1

                        elif csv_header_element == "extension":
                            if headerExtensionIterator < len(oneConcept.extension):
                                csv_line.append(oneConcept.extension[headerExtensionIterator])
                            else:
                                csv_line.append('')
                            headerExtensionIterator += 1

                        elif csv_header_element == "property":
                            if headerPropertyIterator < len(oneConcept.property):
                                csv_line.append(oneConcept.property[headerPropertyIterator])
                            else:
                                csv_line.append('')
                            headerPropertyIterator += 1

                        elif hasattr(oneConcept, csv_header_element) and (tmp := getattr(oneConcept,csv_header_element)):
                            csv_line.append(tmp)#getattr(oneConcept,csv_header_element))
                        else:
                            csv_line.append('')

                    #adding not existing columns because we just got some values
                    if 'code' not in csv_header and (tmp := oneConcept.code):#hasattr(oneConcept,'code'):
                        csv_header.append('code')
                        csv_line.append(tmp)
                    if 'display' not in csv_header and (tmp := oneConcept.display):#hasattr(oneConcept,'display'):
                        csv_header.append('display')
                        csv_line.append(tmp)
                    if 'definition' not in csv_header and (tmp := oneConcept.definition):#hasattr(oneConcept,'definition'):
                        csv_header.append('definition')
                        csv_line.append(tmp)
                    while (conceptDesig := oneConcept.designation) and headerDesignationIterator < len(conceptDesig):
                        csv_header.append('designation')
                        csv_line.append(conceptDesig[headerDesignationIterator])
                        headerDesignationIterator += 1
                    while (conceptExtension := oneConcept.extension) and headerExtensionIterator < len(conceptExtension):
                        csv_header.append('extension')
                        csv_line.append(conceptExtension[headerExtensionIterator])
                        headerExtensionIterator += 1
                    while (conceptProps := oneConcept.property) and headerPropertyIterator < len(conceptProps):
                        csv_header.append('property')
                        csv_line.append(conceptProps[headerPropertyIterator])
                        headerPropertyIterator += 1

                #for ValueSets
                elif self.get_resource() == 'ValueSet':
                    # adding values to already existing columns, if value empty then an empty string is added
                    headerDesignationIterator = 0
                    headerFilterIterator = 0
                    headerExtensionIterator = 0
                    for csv_header_element in csv_header:

                        if csv_header_element == "designation":
                            if headerDesignationIterator < len(oneConcept.designation):
                                csv_line.append(oneConcept.designation[headerDesignationIterator])
                            else:
                                csv_line.append('')
                            headerDesignationIterator += 1

                        elif csv_header_element == "filter":
                            if headerFilterIterator < len(oneConcept.filter):
                                csv_line.append(oneConcept.filter[headerFilterIterator])
                            else:
                                csv_line.append('')
                            headerFilterIterator += 1

                        elif csv_header_element == "extension":
                            extension_found = False
                            while (conceptExtension := oneConcept.extension) and headerExtensionIterator < len(conceptExtension) and not extension_found:
                                # skip conceptOrder if in extensions
                                if not conceptExtension[headerExtensionIterator].url == ext.ConceptOrder.extension_canonical:
                                    csv_line.append(conceptExtension[headerExtensionIterator])
                                    extension_found = True
                                headerExtensionIterator += 1

                            if not extension_found:
                                csv_line.append('')

                        elif self.parenting_by == ParentingBy.conceptOrder and csv_header_element == 'conceptOrder' and (tmp := oneConcept):
                            for one_ext in tmp.extension:
                                if one_ext.url == ext.ConceptOrder.extension_canonical:
                                    if one_ext.value:
                                        csv_line.append(one_ext.value)
                                    break
                            else:
                                # Use index of current concept when generating conceptOrder from scratch.
                                # Add +1 in order to prevent conceptOrder of value '0' as this will be interpreted as 'False' in if-statements
                                csv_line.append(conceptList.index(oneConcept) + 1)
                        elif self.parenting_by == ParentingBy.conceptOrder and csv_header_element == 'parentByConceptOrder' and (tmp := oneConcept.parent):
                            for one_ext in tmp.extension:
                                if one_ext.url == ext.ConceptOrder.extension_canonical:
                                    if one_ext.value:
                                        csv_line.append(one_ext.value)
                                        break
                                    else:
                                        # if parentByConceptOrder is used and the parent concept has got the extension a proper value is required
                                        import sys
                                        sys.exit('If parentByConceptOrder is used and the parent concept uses the extension a proper value is required. See concept with code: ' + tmp.code)
                            else:
                                # Use index of current concept when generating conceptOrder from scratch.
                                # Add +1 in order to prevent conceptOrder of value '0' as this will be interpreted as 'False' in if-statements
                                csv_line.append(conceptList.index(tmp) + 1)
                        elif self.parenting_by == ParentingBy.parentAndSystem and csv_header_element == 'parent' and (tmp := oneConcept.parent):
                            if tmp.code:
                                csv_line.append(tmp.code)
                            else:
                                csv_line.append('')
                        elif self.parenting_by == ParentingBy.parentAndSystem and csv_header_element == 'parentSystem' and (tmp := oneConcept.parent):
                            if tmp.system:
                                csv_line.append(tmp.system)
                            else:
                                csv_line.append('')

                        # elif csv_header_element == 'version':
                        #     # write the system version if existent
                        #     if  tmp := oneConcept.version:
                        #         csv_line.append(tmp)
                            #else:
                                # retrieve version from metadata

                                # # theoretically one_concept.system could be either canonical or OID in
                                # # most cases, however, it would be the canonical
                                # if tmp := self.gainVersionFromCanonical(oneConcept.system):
                                #     csv_line.append(tmp)
                                # elif tmp := self.gainVersionFromOid(oneConcept.system):
                                #     csv_line.append(tmp)
                                # else: # in case no version for the codesystem is available
                                #     csv_line.append('')

                        elif hasattr(oneConcept, csv_header_element) and (tmp := getattr(oneConcept,csv_header_element)):
                            csv_line.append(tmp)#getattr(oneConcept,csv_header_element))
                        else:
                            csv_line.append('')

                    #adding not existing columns because we just got some values
                    if 'system' not in csv_header and (tmp := oneConcept.system):# hasattr(oneConcept,'system'):
                        csv_header.append('system')
                        csv_line.append(tmp)
                    # if 'version' not in csv_header:
                    #     # create 'version' header in any case
                    #     csv_header.append('version')
                        # # write the system version if existent
                        # if  tmp := oneConcept.version:
                        #     csv_line.append(tmp)
                        # else:
                            # retrieve version from metadata

                            # # theoretically one_concept.system could be either canonical or OID in
                            # # most cases, however, it would be the canonical
                            # if tmp := self.gainVersionFromCanonical(oneConcept.system):
                            #     csv_line.append(tmp)
                            # elif tmp := self.gainVersionFromOid(oneConcept.system):
                            #     csv_line.append(tmp)
                            # else: # in case no version for the codesystem is available
                            #     csv_line.append('')
                    if 'code' not in csv_header and (tmp := oneConcept.code):#hasattr(oneConcept,'code'):
                        csv_header.append('code')
                        csv_line.append(tmp)
                    if 'display' not in csv_header and (tmp := oneConcept.display):#hasattr(oneConcept,'display'):
                        csv_header.append('display')
                        csv_line.append(tmp)
                    while (conceptDesig := oneConcept.designation) and headerDesignationIterator < len(conceptDesig):
                        csv_header.append('designation')
                        csv_line.append(conceptDesig[headerDesignationIterator])
                        headerDesignationIterator += 1

                    if self.parenting_by == ParentingBy.conceptOrder and 'conceptOrder' not in csv_header and oneConcept:
                        csv_header.append('conceptOrder')
                        for one_ext in oneConcept.extension:
                            if one_ext.url == ext.ConceptOrder.extension_canonical:
                                if one_ext.value:
                                    csv_line.append(one_ext.value)
                                break
                        else:
                            # Use index of current concept when generating conceptOrder from scratch.
                            # Add +1 in order to prevent conceptOrder of value '0' as this will be interpreted as 'False' in if-statements
                            csv_line.append(conceptList.index(oneConcept) + 1)
                    if self.parenting_by == ParentingBy.conceptOrder and 'parentByConceptOrder' not in csv_header and (tmp := oneConcept.parent):
                        csv_header.append('parentByConceptOrder')
                        for one_ext in tmp.extension:
                            if one_ext.url == ext.ConceptOrder.extension_canonical:
                                if one_ext.value:
                                    csv_line.append(one_ext.value)
                                    break
                                else:
                                    # if parentByConceptOrder is used and the parent concept has got the extension a proper value is required
                                    import sys
                                    sys.exit('If parentByConceptOrder is used and the parent concept uses the extension a proper value is required. See concept with code: ' + tmp.code)
                        else:
                            # Use index of current concept when generating conceptOrder from scratch.
                            # Add +1 in order to prevent conceptOrder of value '0' as this will be interpreted as 'False' in if-statements
                            csv_line.append(conceptList.index(tmp) + 1)
                    if self.parenting_by == ParentingBy.parentAndSystem and 'parent' not in csv_header and (tmp := oneConcept.parent):
                        csv_header.append('parent')
                        if tmp.code:
                            csv_line.append(tmp.code)
                        else:
                            csv_line.append('')
                    if self.parenting_by == ParentingBy.parentAndSystem and 'parentSystem' not in csv_header and (tmp := oneConcept.parent):
                        csv_header.append('parentSystem')
                        if tmp.system:
                            csv_line.append(tmp.system)
                        else:
                            csv_line.append('')

                    while (conceptFilter := oneConcept.filter) and headerFilterIterator < len(conceptFilter):
                        csv_header.append('filter')
                        csv_line.append(conceptFilter[headerFilterIterator])
                        headerFilterIterator += 1
                    # while (conceptExtension := oneConcept.extension) and headerExtensionIterator < len(conceptExtension):
                    #     # skip conceptOrder if in extensions and parentingByConceptOrder is actived, because of possible redundancy
                    #     if not conceptExtension[headerExtensionIterator].url == ext.ConceptOrder.extension_canonical:
                    #         csv_header.append('extension')
                    #         csv_line.append(conceptExtension[headerExtensionIterator])
                    #     headerExtensionIterator += 1
                    if 'abstract' not in csv_header and (tmp := oneConcept.abstract):
                        csv_header.append('abstract')
                        csv_line.append(tmp)
                    if 'inactive' not in csv_header and (tmp := oneConcept.inactive):
                        csv_header.append('inactive')
                        csv_line.append(tmp)
                    if 'exclude' not in csv_header and (tmp := oneConcept.exclude):
                        csv_header.append('exclude')
                        csv_line.append(tmp)


                csv_list.append(csv_line)

            csv_list.insert(0,csv_header)

            if outfile.name.endswith('.csv'):
                # special encoding because of Excel compatibility, see https://stackoverflow.com/questions/6588068/which-encoding-opens-csv-files-correctly-with-excel-on-both-mac-and-windows
                try:
                    writeCSV(outfile, csv_meta, csv_list, file_style=file_style)
                except UnicodeEncodeError:
                    print(" (because of some EncodeError, this terminologys *.1.propcsv.csv will be written in utf-8 instead of cp1252)")
                    writeCSV(outfile, csv_meta, csv_list, encoding="utf-8")

            elif outfile.name.endswith('.xlsx'):
                writeXLSX(outfile, csv_meta, csv_list, file_style=file_style)

def writeCSV(outfile, csv_meta, csv_list, encoding="cp1252", file_style=FileStyle.propCSV):
    outfile_path = outfile.name
    if file_style is FileStyle.spreadCT:
        outfile_path = outfile_path.replace(".data.1.spreadct.csv", ".meta.1.spreadct.csv")
        encoding = "utf-8"

    with open(outfile_path, 'wb') as outfile:
        first_line_of_file = True

        if file_style is FileStyle.propCSV:
            csv_meta = list(zip(*csv_meta)) #transpose the list
        for csvline in csv_meta:
            first_line_of_file = write_csv_line(outfile, encoding, first_line_of_file, csvline)

        if file_style is FileStyle.propCSV:
            outfile.write(("\n").encode(encoding))
        elif file_style is FileStyle.spreadCT:
            outfile.close() # closing and opening with the same name, so that the with is still active, see https://www.geeksforgeeks.org/with-statement-in-python/
            outfile = open(outfile_path.replace(".meta.1.spreadct.csv",".data.1.spreadct.csv"), 'wb')
            first_line_of_file = True
        for csvline in csv_list:
            first_line_of_file = write_csv_line(outfile, encoding, first_line_of_file, csvline)

def write_csv_line(outfile, encoding, first_line_of_file, csvline):
    if first_line_of_file:
        first_line_of_file = False
    else:
        outfile.write(("\n").encode(encoding))
    newline = True
    for csvelement in csvline:
        outfile.write(((";" if not newline else "") + "\"" + str(csvelement).replace("\"",'""') + "\"").encode(encoding))
        newline = False
    return first_line_of_file

def writeXLSX(outfile, csv_meta, csv_list, file_style=FileStyle.propCSV):
    import xlsxwriter
    # Create an new Excel file and add a worksheet.
    workbook = xlsxwriter.Workbook(outfile.name)
    if file_style is FileStyle.propCSV:
        worksheet = workbook.add_worksheet()
        csv_meta = list(zip(*csv_meta))
    elif file_style is FileStyle.spreadCT:
        worksheet_data = workbook.add_worksheet(name="data")
        worksheet_meta = workbook.add_worksheet(name="meta")
        worksheet = worksheet_meta

    # fill with meta
    x = -1
    for csvline in csv_meta: #transpose the list
        x += 1
        y = -1
        for csvelement in csvline:
            y += 1
            """
            based on https://stackoverflow.com/questions/71574319/how-to-remove-newline-characters-so-it-doesnt-produce-x000d-when-reading-exce
            all ocurrences of \r\n will be replaced with a single \n in order to guarantee that reading from xlsx does not produce _x000D_ instead
            of \r
            """
            worksheet.write_string(x,y,str(csvelement).replace('\r\n', '\n'))#.encode(encoding).decode(encoding))

    # for the empty row between meta and data
    x += 1

    if file_style is FileStyle.spreadCT:
        worksheet = worksheet_data
        x = -1 #start from first row

    # fill with data
    for csvline in csv_list:
        x += 1
        y = -1
        for csvelement in csvline:
            y += 1
            """
            based on https://stackoverflow.com/questions/71574319/how-to-remove-newline-characters-so-it-doesnt-produce-x000d-when-reading-exce
            all ocurrences of \r\n will be replaced with a single \n in order to guarantee that reading from xlsx does not produce _x000D_ instead
            of \r
            """
            worksheet.write_string(x,y,str(csvelement).replace('\r\n', '\n'))#.encode(encoding).decode(encoding))

    workbook.close()

class PropCsvAsXLSX():
    def parse(inFilename):
        import pandas as pd

        read_file = pd.read_excel(inFilename, keep_default_na=False, header=None, names=None)
        try:
            read_file.to_csv(inFilename.replace('.1.propcsv.xlsx', '.1.propcsv.csv'), index = None, header=False, sep=';', quoting=1, quotechar='"', encoding='cp1252')
            return PropCsvAndMaster(inFilename.replace('.1.propcsv.xlsx', '.1.propcsv.csv'))
        except UnicodeEncodeError:
            read_file.to_csv(inFilename.replace('.1.propcsv.xlsx', '.1.propcsv.csv'), index = None, header=False, sep=';', quoting=1, quotechar='"', encoding='utf-8')
            return PropCsvAndMaster(inFilename.replace('.1.propcsv.xlsx', '.1.propcsv.csv'), encoding='utf-8')

    def exporto(self, outfile, outputClass, argsLang=None, incHierarchyExt4VS=False):
        PropCsvAndMaster.exporto(self, outfile, outputClass, argsLang, incHierarchyExt4VS)

class SpreadCT(PropCsvAndMaster):
    def parse(inFilename):
        return PropCsvAndMaster(inFilename, encoding='utf-8', file_style=FileStyle.spreadCT)

    def exporto(self, outfile, outputClass, argsLang=None, incHierarchyExt4VS=False):
        PropCsvAndMaster.exporto(self, outfile, outputClass, argsLang, incHierarchyExt4VS, file_style=FileStyle.spreadCT)

class SpreadCTasXLSX():
    def parse(inFilename):
        import pandas as pd

        read_meta = pd.read_excel(inFilename, keep_default_na=False, sheet_name="meta", header=None, names=None)
        read_data = pd.read_excel(inFilename, keep_default_na=False, sheet_name="data", header=None, names=None)

        read_meta.to_csv(inFilename.replace('.1.spreadct.xlsx', '.meta.1.spreadct.csv'), index = None, header=False, sep=';', quoting=1, quotechar='"', encoding='utf-8')
        read_data.to_csv(inFilename.replace('.1.spreadct.xlsx', '.data.1.spreadct.csv'), index = None, header=False, sep=';', quoting=1, quotechar='"', encoding='utf-8')
        ret = SpreadCT.parse(inFilename.replace('.1.spreadct.xlsx', '.data.1.spreadct.csv'))
        # management decision: no spreadct csvs
        import os
        os.remove(inFilename.replace('.1.spreadct.xlsx', '.data.1.spreadct.csv'))
        os.remove(inFilename.replace('.1.spreadct.xlsx', '.meta.1.spreadct.csv'))
        return ret

    def exporto(self, outfile, outputClass, argsLang=None, incHierarchyExt4VS=False):
        SpreadCT.exporto(self, outfile, outputClass, argsLang, incHierarchyExt4VS)

class Resource(enum.Enum):
    ValueSet = 'ValueSet'
    CodeSystem = 'CodeSystem'

class Status(enum.Enum):
    #draft = 'draft'
    active = 'active'
    #retired = 'retired'
    #unknown = 'unknown'

class Type(enum.Enum):
    Extensional ='Extensional'
    Intensional ='Intensional'

class Content(enum.Enum):
    notpresent = 'not-present'
    example = 'example'
    fragment = 'fragment'
    complete = 'complete'
    supplement = 'supplement'

class HierarchyMeaning(enum.Enum):
    groupedby = "grouped-by"
    isa = "is-a"
    partof = "part-of"
    classifiedwith = "classified-with"

class PropertyCodeType(enum.Enum):
    code = "code"
    coding = "Coding"
    string = "string"
    integer = "integer"
    boolean = "boolean"
    datetime = "dateTime"
    decimal = "decimal"

class ContactDetail():
    def __init__(self, name=None, telecom=None):
        self.name = name
        # see https://stackoverflow.com/questions/73714169/why-are-there-two-elements-in-the-collection
        #     https://stackoverflow.com/questions/1132941/least-astonishment-and-the-mutable-default-argument
        #     https://web.archive.org/web/20200221224620id_/http://effbot.org/zone/default-values.htm
        #     https://docs.python.org/3/reference/compound_stmts.html#function-definitions
        if telecom is None:
            telecom = []
        self.telecom = telecom

    @classmethod
    def from_string(cls, str):
        # str is None or empty return None
        if not str:
            return None

        telecom_list = []

        splitted_contact = str.split('|',2)
        name = splitted_contact[0]

        splitted_telecom = splitted_contact[1].split('~')
        for telecom in splitted_telecom:
            if telecom:
                telecom_list.append(cls.ContactPoint.from_string(telecom))

        return cls(name, telecom_list)

    def __str__(self) -> str:
        tmp_str = ""
        if temp := self.name:
            tmp_str += temp
        tmp_str += "|"
        telecom_list = []
        for telecom in self.telecom:
            telecom_list.append(str(telecom))
        tmp_str += "~".join(telecom_list)
        return tmp_str

    class ContactPoint():
        def __init__(self, system=None, value=None, use=None, rank=None, period_start=None, period_end=None):
            self.system = system # ContactPointSystem
            self.value = value
            self.use = use # ContactPointUse
            self.rank = rank
            self.period_start = period_start
            self.period_end = period_end

        @classmethod
        def from_string(cls, str):
            splitted_contact_point = str.split('^',6)
            return cls(cls.ContactPointSystem(splitted_contact_point[0]) if splitted_contact_point[0] else None, splitted_contact_point[1], cls.ContactPointUse(splitted_contact_point[2]) if splitted_contact_point[2] else None, splitted_contact_point[3], splitted_contact_point[4], splitted_contact_point[5])

        def __str__(self) -> str:
            tmp_str = ""
            if tmp := self.system:
                tmp_str += tmp.value
            tmp_str += "^"
            if tmp := self.value:
                tmp_str += tmp
            tmp_str += "^"
            if tmp := self.use:
                tmp_str += tmp.value
            tmp_str += "^"
            if tmp := self.rank:
                tmp_str += tmp
            tmp_str += "^"
            if tmp := self.period_start:
                tmp_str += tmp
            tmp_str += "^"
            if tmp := self.period_end:
                tmp_str += tmp
            return tmp_str

        class ContactPointSystem(enum.Enum):
            phone = "phone"
            fax = "fax"
            email = "email"
            pager = "pager"
            url = "url"
            sms = "sms"
            other = "other"

        class ContactPointUse(enum.Enum):
            home = "home"
            work = "work"
            temp = "temp"
            old = "old"
            mobile = "mobile"

class Filter():
    def __init__(self):
        self.code = None
        self.description = None
        self.operator = None
        self.value = None

class Property():
    def __init__(self, code=None, uri=None, description=None, type=PropertyCodeType, idInFsh=None):
        self.code = code
        self.uri = uri
        self.description = description
        self.type = type
        self.idInFsh = idInFsh

    def __str__(self) -> str:
        return self.code+"|"+(self.uri or "")+"|"+(self.description or "")+"|"+self.type.value

class CSConcept():
    def __init__(self):
        self.code = None
        self.display = None
        self.definition = None
        self.designation = []
        self.property = []

    class ConceptProperty():
        def __init__(self, str=None):
            if not str:
                self.code = None
                self.value = None
                self.type = PropertyCodeType
            else:
                propertyElements = str.split('|')
                self.code = propertyElements[0]
                self.type = PropertyCodeType[propertyElements[2].lower()]
                if self.type == PropertyCodeType.coding:
                    self.value = Coding(propertyElements[1])
                else:
                    self.value = propertyElements[1]

        # only used for sort
        def get_value_if_code_is_child(self):
            if self.code == "child":
                return str(self.value)
            else:
                return str(0)

        # only used for sort
        def get_code(self):
            return self.code

        def __str__(self) -> str:
            buildStr = ""
            buildStr = self.code
            buildStr += "|"+ str(self.value)
            buildStr += "|"+ self.type.value
            return buildStr

class VSConcept():
    def __init__(self) -> None:
    # heisst bei uns Codesystem
        self.system = None
        # haben wir nicht, aber dafür von bis datum
        # für FHIR
        self.version = None
        self.code = None
        # heisst bei uns displayName
        self.display = None
        self.designation = [] # ConceptDesignation
        self.filter = [] # VSConceptFilter
        # für FHIR
        self.exclude = False
        self.parent = None
        self.member = [] # simple hirarchie that stores all children, needs a extension
        self.level = 0 # Additional information in what level the concept can be found.
                       # The default value implies that this concept is a top level concept.
                       # For concepts in hierarchies this value would have to be set explicitly.
        self.abstract = False
        self.inactive = False
        self.extension = []

    # created only for sorting, shouldn't be used for anything else
    def get_system(self):
        return self.system

    # created only for sorting, shouldn't be used for anything else
    def get_code(self):
        return self.code

    class VSConceptFilter():
        def __init__(self):
            self.property = None
            self.op = None
            self.value = None

        def __str__(self) -> str:
            buildStr = ""
            if hasattr(self, 'property'): buildStr = self.property
            if hasattr(self, 'op'):
                if buildStr != "":
                    buildStr += "|"
                buildStr += self.op
            if hasattr(self, 'value'):
                if buildStr != "":
                    buildStr += "|"
                buildStr += self.value
            return buildStr

class ConceptDesignation():
    def __init__(self, str=None):
        if str is None:
            self.language = None
            self.use = Coding()
            self.value = None
        else:
            splittedValue = str.split('|',2)
            self.language = splittedValue[0]
            self.use = Coding(splittedValue[1])
            self.value = splittedValue[2]

    def __str__(self) -> str:
        buildStr = ""
        if self.language: buildStr += self.language
        buildStr += "|"
        if self.use: buildStr += str(self.use)
        buildStr += "|"
        if self.value: buildStr += self.value
        return buildStr

class Coding():
    def __init__(self, str=None):
        if not str:
            self.system = None
            self.version = None
            self.code = None
            self.display = None
            self.userSelected = None
        else:
            splittedValue = str.split('^',4)
            self.system = splittedValue[0] or None
            self.version = splittedValue[1] or None
            self.code = splittedValue[2] or None
            self.display = splittedValue[3] or None
            self.userSelected = splittedValue[4] or None

    def __str__(self) -> str:
        buildStr = ""
        if self.system: buildStr += self.system
        buildStr += "^"
        if self.version: buildStr += self.version
        buildStr += "^"
        if self.code: buildStr += self.code
        buildStr += "^"
        if self.display: buildStr += self.display
        buildStr += "^"
        if self.userSelected: buildStr += self.userSelected
        return buildStr

class Extension():
    def __init__(self, str=None):
        if not str:
            self.url = None
            self.value = None
            self.type = ExtensionType
        else:
            extension_elements = str.split('|')
            self.url = extension_elements[0]
            self.type = ExtensionType(extension_elements[2])
            if self.type == ExtensionType.coding:
                self.value = Coding(extension_elements[1])
            elif self.type == ExtensionType.integer:
                self.value = int(extension_elements[1])
            else:
                self.value = extension_elements[1]

    def __str__(self) -> str:
        return self.url + "|" + (str(self.value) if self.value else "") + "|" + self.type.value

# upper case first character: https://stackoverflow.com/questions/3840843/how-to-downcase-the-first-character-of-a-string
first_char_to_upper = lambda s: s[:1].upper() + s[1:] if s else ''
# lower case first character: https://stackoverflow.com/questions/3840843/how-to-downcase-the-first-character-of-a-string
first_char_to_lower = lambda s: s[:1].lower() + s[1:] if s else ''